package com.syriackurien.buyorrent

import com.syriackurien.buyorrent.businesslogic.Analysis
import com.syriackurien.buyorrent.home.protocols.AnalysisParameters
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun calculateMonthlyAmortization_isCorrect(){
        val analysisParameters = AnalysisParameters()
        analysisParameters.price = 1250000f
        analysisParameters.interest = 1.5f
        analysisParameters.mortgageDuration = 30.0f
        val amount = Analysis.calculateMonthlyAmortization(analysisParameters)
        assertEquals(4314f, amount, 0.1f)
    }
}
