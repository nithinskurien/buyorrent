package com.syriackurien.buyorrent.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface FieldDatabaseDao {

    @Insert
    fun insert(entry: FieldValue)

    @Update
    fun update(entry: FieldValue)

    @Query("SELECT * from field_table WHERE pref_id = :key")
    fun getValues(key: Long): FieldValue?

    @Query("SELECT * from field_table WHERE pref_id = :key")
    fun getPreferenceLive(key: Long): LiveData<FieldValue>

    @Query("SELECT * from field_table WHERE pref_id = '1'")
    fun getActiveValues(): LiveData<FieldValue>

}