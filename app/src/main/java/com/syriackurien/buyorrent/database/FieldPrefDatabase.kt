package com.syriackurien.buyorrent.database

import android.app.Application
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.syriackurien.buyorrent.main.HomeConstants
import java.util.concurrent.Executors

@Database(entities = [SavedPreference::class, FieldValue::class], version = 2, exportSchema = false)
abstract class FieldPrefDatabase : RoomDatabase() {

    abstract val fPreferenceDatabaseDao: PreferenceDatabaseDao
    abstract val fFieldDatabaseDao: FieldDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: FieldPrefDatabase? = null

        fun getInstance(context: Context): FieldPrefDatabase {

            synchronized(this) {

                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        FieldPrefDatabase::class.java,
                        "Field_Perf_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                Executors.newSingleThreadExecutor().execute {
                                    val prefId = getInstance(context).fPreferenceDatabaseDao.insert(HomeConstants.getDefaultPrefEntry())
                                    getInstance(context).fFieldDatabaseDao.insert(HomeConstants.getDefaultFieldEntry(
                                        context.applicationContext as Application, prefId))
                                }
                            }
                        })
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
