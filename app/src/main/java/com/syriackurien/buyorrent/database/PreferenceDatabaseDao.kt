package com.syriackurien.buyorrent.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface PreferenceDatabaseDao {

    @Insert
    fun insert(entry: SavedPreference) : Long

    @Update
    fun update(entry: SavedPreference)

    @Query("SELECT * from saved_pref_table WHERE pref_id = :key")
    fun getPreference(key: Long): SavedPreference?

    @Query("DELETE FROM saved_pref_table WHERE pref_id = :key")
    fun deletePreference(key: Long)

    @Query("DELETE FROM saved_pref_table")
    fun deleteAll()

    @Query("SELECT * FROM saved_pref_table ORDER BY pref_id DESC")
    fun getAllPreference(): LiveData<List<SavedPreference>>

    @Query("SELECT * FROM saved_pref_table ORDER BY pref_id DESC LIMIT 1")
    fun getLatestPreference(): SavedPreference?

    @Query("SELECT * from saved_pref_table WHERE pref_id = :key")
    fun getPreferenceLive(key: Long): LiveData<SavedPreference>

    @Query("SELECT * from saved_pref_table WHERE current_selection = '1'")
    fun getActivePreference(): SavedPreference

}