package com.syriackurien.buyorrent.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName= "saved_pref_table")
data class SavedPreference (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "pref_id")
    var entryId: Long = 0L,

    @ColumnInfo(name = "pref_value")
    var entryName: String = "",

    @ColumnInfo(name = "pref_time_milli")
    val entryTimeMilli: Long = System.currentTimeMillis(),

    @ColumnInfo(name = "current_selection")
    var isDefault: Boolean = false
)

@Entity(
    tableName = "field_table",
    foreignKeys = [ForeignKey(
        entity = SavedPreference::class,
        parentColumns = arrayOf("pref_id"),
        childColumns = arrayOf("pref_id"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class FieldValue (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "field_id")
    var fieldId: Long = 0L,

    @ColumnInfo(name = "pref_id")
    var entryId: Long,

    @ColumnInfo(name = "field_value")
    var fieldValue: String = "",

    @ColumnInfo(name = "field_location")
    var fieldLocation: String = ""
)