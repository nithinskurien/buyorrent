package com.syriackurien.buyorrent.main


import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.syriackurien.buyorrent.database.FieldDatabaseDao
import com.syriackurien.buyorrent.database.PreferenceDatabaseDao
import com.syriackurien.buyorrent.home.protocols.PageListContent
import com.syriackurien.buyorrent.util.*
import kotlinx.coroutines.*

class HomeViewModel(val fFieldSource: FieldDatabaseDao,
                    val fPerfSource : PreferenceDatabaseDao,
                    val fApplication: Application) : AndroidViewModel(fApplication) {

    var fFieldListContent: MutableLiveData<MutableList<PageListContent>> =
    MutableLiveData(HomeConstants.getPageListContent(fApplication).toMutableList())
    private var _fFieldListCompare = fFieldListContent.clone()
    private var _fViewModelJob = Job()
    private val _fUiScope = CoroutineScope(Dispatchers.Main + _fViewModelJob)
    private var _fNavigateToAnalysis = MutableLiveData<Boolean>()
    val fNavigateToAnalysis: LiveData<Boolean>
        get() = _fNavigateToAnalysis
    private var _fNavigateToHome = MutableLiveData<Boolean>()
    val fNavigateToHome: LiveData<Boolean>
        get() = _fNavigateToHome

    private suspend fun getActivePrefFromDatabase() {
        withContext(Dispatchers.IO) {
            val defaultID = fPerfSource.getActivePreference().entryId
            val fieldValue = fFieldSource.getValues(defaultID)
            if (fieldValue != null) {
                fFieldListContent.value?.setValues(fieldValue.fieldValue)
                fFieldListContent.value?.setPositions(fieldValue.fieldLocation)

            }
        }
    }

    fun setFragmentsRecyclerList() {
        _fUiScope.launch {
            getActivePrefFromDatabase()
        }
    }

    fun setOnChangePageList(pageListContent: PageListContent){
        Toast.makeText(fApplication, pageListContent.toString(), Toast.LENGTH_LONG).show()
        fFieldListContent.value?.filter { it.id == pageListContent.id }?.
        get(0)?.value = pageListContent.value
        fFieldListContent.value = fFieldListContent.value
        }

    override fun onCleared() {
        super.onCleared()
        _fViewModelJob.cancel()
    }

    fun navigateToAnalysis(){
        _fNavigateToAnalysis.value = true
        fFieldListContent.value?.copy(_fFieldListCompare.value!!)
    }

    fun doneNavigatingAnalysis(){
        _fNavigateToAnalysis.value = null
    }

    fun navigateToHome(){
        _fNavigateToHome.value = true
    }

    fun doneNavigatingHome(){
        _fNavigateToHome.value = null
    }

}
