package com.syriackurien.buyorrent.main

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.syriackurien.buyorrent.database.FieldDatabaseDao
import com.syriackurien.buyorrent.database.PreferenceDatabaseDao

class HomeViewModelFactory (
    private val fFieldSource: FieldDatabaseDao,
    private val fPrefSource: PreferenceDatabaseDao,
    private val fApplication: Application
    ) : ViewModelProvider.Factory {
        @Suppress("unchecked_cast")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                return HomeViewModel(fFieldSource, fPrefSource, fApplication) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
}