package com.syriackurien.buyorrent.main

import android.app.Application
import com.syriackurien.buyorrent.R
import com.syriackurien.buyorrent.database.FieldValue
import com.syriackurien.buyorrent.database.SavedPreference
import com.syriackurien.buyorrent.home.protocols.PageListContent
import com.syriackurien.buyorrent.util.getListCSV

object HomeConstants {

    const val defaultPrefName: String = "default"
    const val pageKey: String = "Page"

    enum class Page(val title: String, val position: Int) {
        LEFT("LEFT", 0),
        HOME("HOME", 1),
        RIGHT("RIGHT", 2),
        INVALID("INVALID", -1)
    }

    enum class pageListID(val id : Long){
        PRICE(0),
        MORTGAGE_DURATION(1),
        INTEREST(2),
        DOWNPAYMENT(3),
        OWNERSHIP_DURATION(4),
        PRICE_GROWTH_DURATION(5),
        RENT_GROWTH_RATE(6),
        INVESTMENT_GROWTH_RATE(7),
        INFLATION_RATE(8),
        PROPERTY_TAX_RATE(9),
        INCOME_TAX_RATE(10),
        BUYING_HOME_RATE(11),
        SELLING_HOME_RATE(12),
        MAINTENANCE_RATE(13),
        HOME_OWNER_INSURANCE(14),
        OWNER_UTILITY_COST(15),
        TAX_DEDUCTION_RATE(16),
        SECURITY_DEPOSIT(17),
        BROKER_FEE(18),
        RENTER_INSURANCE(19),
        RENT_UTILITY_COST(20),

    }

    fun getPageListContent(application: Application) : List<PageListContent>{

        return listOf(
            PageListContent(pageListID.PRICE.id, application.getString(R.string.field0_name), 0, application.getString(R.string.field0_desc), 1000000f, Page.HOME.position),
            PageListContent(pageListID.MORTGAGE_DURATION.id, application.getString(R.string.field1_name), 1, application.getString(R.string.field1_desc), 30f, Page.HOME.position),
            PageListContent(pageListID.INTEREST.id, application.getString(R.string.field2_name), 2, application.getString(R.string.field2_desc), 1.5f, Page.HOME.position),
            PageListContent(pageListID.DOWNPAYMENT.id, application.getString(R.string.field3_name), 3, application.getString(R.string.field3_desc), 15f, Page.HOME.position),
            PageListContent(pageListID.OWNERSHIP_DURATION.id, application.getString(R.string.field4_name), 4, application.getString(R.string.field4_desc), 10f, Page.HOME.position),
            PageListContent(pageListID.PRICE_GROWTH_DURATION.id, application.getString(R.string.field5_name), 5, application.getString(R.string.field5_desc), 3f, Page.LEFT.position),
            PageListContent(pageListID.RENT_GROWTH_RATE.id, application.getString(R.string.field6_name), 6, application.getString(R.string.field6_desc), 2f, Page.RIGHT.position),
            PageListContent(pageListID.INVESTMENT_GROWTH_RATE.id, application.getString(R.string.field7_name), 7, application.getString(R.string.field7_desc), 5f, Page.RIGHT.position),
            PageListContent(pageListID.INFLATION_RATE.id, application.getString(R.string.field8_name), 8, application.getString(R.string.field8_desc), 2f, Page.RIGHT.position),
            PageListContent(pageListID.PROPERTY_TAX_RATE.id, application.getString(R.string.field9_name), 9, application.getString(R.string.field9_desc), 1f, Page.LEFT.position),
            PageListContent(pageListID.INCOME_TAX_RATE.id, application.getString(R.string.field10_name), 10, application.getString(R.string.field10_desc), 30f, Page.RIGHT.position),
            PageListContent(pageListID.BUYING_HOME_RATE.id, application.getString(R.string.field11_name), 11, application.getString(R.string.field11_desc), 4f, Page.LEFT.position),
            PageListContent(pageListID.SELLING_HOME_RATE.id, application.getString(R.string.field12_name), 12, application.getString(R.string.field12_desc), 4f, Page.LEFT.position),
            PageListContent(pageListID.MAINTENANCE_RATE.id, application.getString(R.string.field13_name), 13, application.getString(R.string.field13_desc), 1f, Page.LEFT.position),
            PageListContent(pageListID.HOME_OWNER_INSURANCE.id, application.getString(R.string.field14_name), 14, application.getString(R.string.field14_desc), 0.5f, Page.LEFT.position),
            PageListContent(pageListID.OWNER_UTILITY_COST.id, application.getString(R.string.field15_name), 15, application.getString(R.string.field15_desc), 100f, Page.LEFT.position),
            PageListContent(pageListID.TAX_DEDUCTION_RATE.id, application.getString(R.string.field16_name), 16, application.getString(R.string.field16_desc), 30f, Page.LEFT.position),
            PageListContent(pageListID.SECURITY_DEPOSIT.id, application.getString(R.string.field17_name), 17, application.getString(R.string.field17_desc), 1.6f, Page.RIGHT.position),
            PageListContent(pageListID.BROKER_FEE.id, application.getString(R.string.field18_name), 18, application.getString(R.string.field18_desc), 0f, Page.RIGHT.position),
            PageListContent(pageListID.RENTER_INSURANCE.id, application.getString(R.string.field19_name), 19, application.getString(R.string.field19_desc), 0f, Page.RIGHT.position),
            PageListContent(pageListID.RENT_UTILITY_COST.id, application.getString(R.string.field20_name), 20, application.getString(R.string.field20_desc), 0f, Page.RIGHT.position)
        )
    }

    fun getDefaultPrefEntry(): SavedPreference{

        val defaultPref = SavedPreference()
        defaultPref.entryName = defaultPrefName
        defaultPref.isDefault = true

        return defaultPref
    }

    fun getDefaultFieldEntry(application: Application, perfId: Long): FieldValue{
        val defaultField = FieldValue(entryId = perfId)
        defaultField.fieldLocation = getPageListContent(application).getListCSV("position")
        defaultField.fieldValue = getPageListContent(application).getListCSV("value")
        return defaultField
    }
}