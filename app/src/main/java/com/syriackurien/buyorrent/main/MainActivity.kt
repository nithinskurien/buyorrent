package com.syriackurien.buyorrent.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.syriackurien.buyorrent.database.FieldPrefDatabase
import com.syriackurien.buyorrent.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val application = requireNotNull(application)
        val fieldSource = FieldPrefDatabase.getInstance(application).fFieldDatabaseDao
        val prefSource = FieldPrefDatabase.getInstance(application).fPreferenceDatabaseDao

        val viewModelFactory = HomeViewModelFactory(fieldSource, prefSource, application)
        homeViewModel =  this.let { ViewModelProvider(it, viewModelFactory).get(HomeViewModel::class.java)}
        supportActionBar?.setDisplayShowTitleEnabled(false);
    }
}
