package com.syriackurien.buyorrent.businesslogic

import com.syriackurien.buyorrent.home.protocols.AnalysisParameters
import com.syriackurien.buyorrent.home.protocols.PageListContent
import kotlin.math.pow

class Analysis {

    private lateinit var _fPageContentList: PageListContent
    private lateinit  var _fPageContentListCopy: PageListContent

    fun init (pageListContent: PageListContent){
        _fPageContentList = pageListContent
        _fPageContentListCopy = pageListContent
    }
    companion object {
        fun calculateMonthlyAmortization(analysisParameters: AnalysisParameters) : Float{
            val interestRatio = analysisParameters.interest / 100
            val denPower = (1 + (interestRatio/12)).pow(-12 * analysisParameters.mortgageDuration)
            return (analysisParameters.price * interestRatio)/(12 * (1- denPower))
        }
    }

}