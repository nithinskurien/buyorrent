package com.syriackurien.buyorrent.util

import androidx.lifecycle.MutableLiveData
import com.syriackurien.buyorrent.home.protocols.AnalysisParameters
import com.syriackurien.buyorrent.home.protocols.PageListContent
import com.syriackurien.buyorrent.main.HomeConstants
import com.syriackurien.buyorrent.main.HomeConstants.Page
import java.util.*

fun List<PageListContent>.getHomeList() : List<PageListContent>{
    return this.filter { it.screenPosition == Page.HOME.position }
}

fun List<PageListContent>.getLeftList() : List<PageListContent>{
    return this.filter { it.screenPosition == Page.LEFT.position }
}

fun List<PageListContent>.getRightList() : List<PageListContent>{
    return this.filter { it.screenPosition == Page.RIGHT.position }
}

fun List<PageListContent>.getListCSV(prop: String): String {
    return when (prop.toLowerCase(Locale.ROOT)) {
        "value" ->
            this.map { it.value }.joinToString(separator = ", ")
        "position" ->
            this.map { it.screenPosition }.joinToString(separator = ", ")
        else ->
            ""
    }
}

fun MutableList<PageListContent>.setValues(values: String){
   val floatValues = values.split(",").map { it.trim().toFloat() }
    this.forEachIndexed { index, pageListContent ->
        pageListContent.value = floatValues[index]
    }
}

fun MutableList<PageListContent>.setPositions(positions: String){
    val intValues = positions.split(",").map { it.trim().toInt() }
    this.forEachIndexed { index, pageListContent ->
        pageListContent.screenPosition = intValues[index]
    }
}

fun MutableLiveData<MutableList<PageListContent>>.clone():  MutableLiveData<MutableList<PageListContent>>{
    val pageListContentClone =  MutableLiveData<MutableList<PageListContent>>()
    pageListContentClone.value = mutableListOf()
    this.value?.forEach {
        pageListContentClone.value!!.add(it.copy())
    }
    return pageListContentClone
}

fun MutableList<PageListContent>.clone():  MutableList<PageListContent>{
    val pageListContentClone =  mutableListOf<PageListContent>()
    this.forEach {
        pageListContentClone.add(it.copy())
    }
    return pageListContentClone
}

fun MutableList<PageListContent>.copy( dest :MutableList<PageListContent>) {
    if(this.size == dest.size) {
        this.forEach { srcPageListContent ->
            dest.forEach { destPageListContent ->
                if (srcPageListContent.id == destPageListContent.id) {
                    destPageListContent.value = srcPageListContent.value
                    destPageListContent.screenPosition = srcPageListContent.screenPosition
                    return
                }
            }
        }
    }
    else{
        throw IllegalArgumentException("Incompatible size")
    }

    fun MutableList<PageListContent>.extractAnalysisParameters() : AnalysisParameters {
        val analysisParameters = AnalysisParameters()
        this.forEach { pageListContent ->
            when(pageListContent.id){
                HomeConstants.pageListID.PRICE.id ->
                    analysisParameters.price = pageListContent.value!!
                HomeConstants.pageListID.MORTGAGE_DURATION.id ->
                    analysisParameters.mortgageDuration = pageListContent.value!!
                HomeConstants.pageListID.INTEREST.id ->
                    analysisParameters.interest = pageListContent.value!!
                HomeConstants.pageListID.DOWNPAYMENT.id ->
                    analysisParameters.downPayment = pageListContent.value!!
                HomeConstants.pageListID.OWNERSHIP_DURATION.id ->
                    analysisParameters.ownershipDuration = pageListContent.value!!
                HomeConstants.pageListID.PRICE_GROWTH_DURATION.id ->
                    analysisParameters.priceGrowthRate = pageListContent.value!!
                HomeConstants.pageListID.RENT_GROWTH_RATE.id ->
                    analysisParameters.rentGrowthRate = pageListContent.value!!
                HomeConstants.pageListID.INVESTMENT_GROWTH_RATE.id ->
                    analysisParameters.investmentGrowthRate = pageListContent.value!!
                HomeConstants.pageListID.INFLATION_RATE.id ->
                    analysisParameters.inflationRate = pageListContent.value!!
                HomeConstants.pageListID.PROPERTY_TAX_RATE.id ->
                    analysisParameters.propertyTaxRate = pageListContent.value!!
                HomeConstants.pageListID.INCOME_TAX_RATE.id ->
                    analysisParameters.incomeTaxRate = pageListContent.value!!
                HomeConstants.pageListID.BUYING_HOME_RATE.id ->
                    analysisParameters.buyingHomeRate = pageListContent.value!!
                HomeConstants.pageListID.SELLING_HOME_RATE.id ->
                    analysisParameters.sellingHomeRate = pageListContent.value!!
                HomeConstants.pageListID.MAINTENANCE_RATE.id ->
                    analysisParameters.maintenanceCost = pageListContent.value!!
                HomeConstants.pageListID.HOME_OWNER_INSURANCE.id ->
                    analysisParameters.homeOwnerInsurance = pageListContent.value!!
                HomeConstants.pageListID.OWNER_UTILITY_COST.id ->
                    analysisParameters.ownerUtilityCost = pageListContent.value!!
                HomeConstants.pageListID.TAX_DEDUCTION_RATE.id ->
                    analysisParameters.taxDeductionRate = pageListContent.value!!
                HomeConstants.pageListID.SECURITY_DEPOSIT.id ->
                    analysisParameters.securityDeposit = pageListContent.value!!
                HomeConstants.pageListID.BROKER_FEE.id ->
                    analysisParameters.brokerFee = pageListContent.value!!
                HomeConstants.pageListID.RENTER_INSURANCE.id ->
                    analysisParameters.renterInsurance = pageListContent.value!!
                HomeConstants.pageListID.RENT_UTILITY_COST.id ->
                    analysisParameters.rentUtilityCost = pageListContent.value!!

            }
        }
        return analysisParameters
    }
}