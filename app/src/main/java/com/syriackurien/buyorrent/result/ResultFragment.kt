package com.syriackurien.buyorrent.result

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.syriackurien.buyorrent.R
import com.syriackurien.buyorrent.databinding.ResultFragmentBinding
import com.syriackurien.buyorrent.home.HomeFragmentDirections
import com.syriackurien.buyorrent.main.HomeViewModel


class ResultFragment : Fragment() {

    private lateinit var _fViewModel: HomeViewModel
    private lateinit var _fBinding: ResultFragmentBinding
    private var _fContext = this

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _fBinding = ResultFragmentBinding.inflate(inflater)
        return _fBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        _fViewModel = activity?.let { ViewModelProvider(it).get(HomeViewModel::class.java) }!!
        _fBinding.apply {
            lifecycleOwner = _fContext
            homeViewModel = _fViewModel
        }
        _fViewModel.fNavigateToHome.observe(viewLifecycleOwner, Observer { toHome ->
            toHome?.let {
                this.findNavController().navigate(ResultFragmentDirections.actionResultFragmentToHomeFragment())
                _fViewModel.doneNavigatingHome()
            }
        })

    }

}
