package com.syriackurien.buyorrent.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.syriackurien.buyorrent.databinding.HomeFragmentBinding
import com.syriackurien.buyorrent.main.HomeConstants
import com.syriackurien.buyorrent.main.HomeViewModel


class HomeFragment : Fragment() {

    private lateinit var _fViewModel: HomeViewModel
    private lateinit var _fBinding: HomeFragmentBinding
    private var _fContext = this

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _fBinding = HomeFragmentBinding.inflate(inflater)
        return _fBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        _fViewModel = activity?.let { ViewModelProvider(it).get(HomeViewModel::class.java) }!!
        val pageAdapter = HomePageAdapter(this)
        pageAdapter.addFragment(ListFragment.newInstance(HomeConstants.Page.LEFT.title))
        pageAdapter.addFragment(ListFragment.newInstance(HomeConstants.Page.HOME.title))
        pageAdapter.addFragment(ListFragment.newInstance(HomeConstants.Page.RIGHT.title))

        _fBinding.apply {
            lifecycleOwner = _fContext
            homeViewModel = _fViewModel
            pageList.apply {
                adapter = pageAdapter
                currentItem = HomeConstants.Page.HOME.position
            }

        }
        _fViewModel.fNavigateToAnalysis.observe(viewLifecycleOwner, Observer { toAnalysis ->
            toAnalysis?.let {
               this.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToResultFragment())
                _fViewModel.doneNavigatingAnalysis()
            }
    })
    }

}
