package com.syriackurien.buyorrent.home.protocols

import android.provider.Telephony

interface IHomeAdapterItems {

    // Returns the list of Adapter List Content for the middle home page
    fun getHomeItemList(): List<PageListContent>

    // Returns the list of Adapter List Content for the left home page
    fun getLeftItemList(): List<PageListContent>

    // Returns the list of Adapter List Content for the right home page
    fun getRightItemList(): List<PageListContent>
}

data class PageListContent(
    val id : Long? = null,
    val name : String? = null,
    val image : Int? = null,
    val description : String? = null,
    var value : Float? = null,
    var screenPosition : Int? = null
)

data class AnalysisParameters(
    var price : Float = 0.0f,
    var mortgageDuration : Float = 0.0f,
    var interest : Float = 0.0f,
    var downPayment : Float = 0.0f,
    var ownershipDuration : Float = 0.0f,
    var priceGrowthRate : Float = 0.0f,
    var rentGrowthRate : Float = 0.0f,
    var investmentGrowthRate : Float = 0.0f,
    var inflationRate : Float = 0.0f,
    var propertyTaxRate : Float = 0.0f,
    var incomeTaxRate : Float = 0.0f,
    var buyingHomeRate : Float = 0.0f,
    var sellingHomeRate : Float = 0.0f,
    var maintenanceCost : Float = 0.0f,
    var homeOwnerInsurance : Float = 0.0f,
    var ownerUtilityCost : Float = 0.0f,
    var taxDeductionRate : Float = 0.0f,
    var securityDeposit : Float = 0.0f,
    var brokerFee : Float = 0.0f,
    var renterInsurance : Float = 0.0f,
    var rentUtilityCost : Float = 0.0f
)
