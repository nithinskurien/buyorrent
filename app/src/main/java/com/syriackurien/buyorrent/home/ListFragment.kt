package com.syriackurien.buyorrent.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.syriackurien.buyorrent.databinding.ListFragmentBinding
import com.syriackurien.buyorrent.home.protocols.PageListContent
import com.syriackurien.buyorrent.main.HomeConstants
import com.syriackurien.buyorrent.main.HomeViewModel
import com.syriackurien.buyorrent.util.getHomeList
import com.syriackurien.buyorrent.util.getLeftList
import com.syriackurien.buyorrent.util.getRightList


class ListFragment : Fragment() {

    private lateinit var _fPage: String
    private lateinit var _fViewModel: HomeViewModel

    companion object {
        fun newInstance(page: String): ListFragment{
            val bundle = Bundle()
            bundle.putString(HomeConstants.pageKey, page)
            val fragment = ListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun readBundle(bundle: Bundle?) {
        if (bundle != null) {
            _fPage = bundle.getString(HomeConstants.pageKey).toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val context = this
        val binding = ListFragmentBinding.inflate(inflater)
        _fViewModel = activity?.let { ViewModelProvider(it).get(HomeViewModel::class.java) }!!
        readBundle(requireArguments())
        val listAdapter = HomeRecyclerAdapter(PageListContentListener {
                pageListContent ->  _fViewModel.setOnChangePageList(pageListContent)
        })
        _fViewModel.setFragmentsRecyclerList()

        binding.apply {
            lifecycleOwner = context

            cardList.apply {
                adapter = listAdapter
                layoutManager = LinearLayoutManager(activity)
            }
        }
        _fViewModel.fFieldListContent.observe(viewLifecycleOwner, Observer {
            it?.let{
                Toast.makeText(_fViewModel.fApplication, "Value Changed", Toast.LENGTH_LONG).show()
                setRecyclerList(listAdapter, it)
            }
        })

        return binding.root
    }

    private fun setRecyclerList(listAdapter: HomeRecyclerAdapter,
                                list: MutableList<PageListContent>){
        when (_fPage){
            HomeConstants.Page.LEFT.title ->
                        listAdapter.submitList(list.getLeftList())
            HomeConstants.Page.HOME.title ->
                        listAdapter.submitList(list.getHomeList())
            HomeConstants.Page.RIGHT.title ->
                        listAdapter.submitList(list.getRightList())
            else -> throw IllegalArgumentException("Incorrect Value Received : $_fPage")
        }
    }



}