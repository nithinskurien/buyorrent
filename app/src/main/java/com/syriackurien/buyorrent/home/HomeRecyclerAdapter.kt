package com.syriackurien.buyorrent.home

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.syriackurien.buyorrent.databinding.CardListEntryBinding
import com.syriackurien.buyorrent.home.protocols.PageListContent


class HomeRecyclerAdapter(val changeListener: PageListContentListener): ListAdapter <PageListContent, HomeRecyclerAdapter.ViewHolder> (HomeListContentDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent, changeListener)
    }

    class ViewHolder(private val binding: CardListEntryBinding,val changeListener: PageListContentListener): RecyclerView.ViewHolder(binding.root){

        fun bind(item: PageListContent) {
            binding.pageListContent = item
            binding.executePendingBindings()
            binding.fieldEditText.onFocusChangeListener = View.OnFocusChangeListener{view: View?, hasFocus: Boolean ->
                if (!hasFocus){
                    item.value = binding.fieldEditText.editableText.toString().toFloat()
                    changeListener.onChange(item)
                    hideKeyboard(view)
                }
            }

        }
        companion object {
            fun from(parent: ViewGroup, changeListener: PageListContentListener): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CardListEntryBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding, changeListener)
            }

            fun hideKeyboard(view: View?) {
                if (view != null) {
                val inputMethodManager: InputMethodManager = view.context.getSystemService(
                    Activity.INPUT_METHOD_SERVICE
                ) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(
                        view.windowToken, 0
                    )
                }
            }
        }

    }

}

class HomeListContentDiffCallback : DiffUtil.ItemCallback<PageListContent>() {

    override fun areItemsTheSame(oldItem: PageListContent, newItem: PageListContent): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PageListContent, newItem: PageListContent): Boolean {
        return oldItem == newItem
    }

}

class PageListContentListener(val changeListener: (pageContent: PageListContent) -> Unit) {
    fun onChange(pageListContent: PageListContent) = changeListener(pageListContent)
}
