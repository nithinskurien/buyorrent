package com.syriackurien.buyorrent.home.testdata

import com.syriackurien.buyorrent.home.protocols.PageListContent
import com.syriackurien.buyorrent.home.protocols.IHomeAdapterItems
import com.syriackurien.buyorrent.main.HomeConstants.Page

class HomeListContentGenerator : IHomeAdapterItems {

    override fun getHomeItemList(): List<PageListContent> {
        return listOf(
            PageListContent(0, "Price",0,"Price of Home",0f, Page.HOME.position),
            PageListContent(1, "Duration",1,"Duration of Mortgage",0f, Page.HOME.position),
            PageListContent(2, "Downpayment",2,"Downpayment of Home in Percentage",0f, Page.HOME.position),
            PageListContent(3, "Interest",3,"Interest in Percentage",0f, Page.HOME.position)
        )
    }

    override fun getLeftItemList(): List<PageListContent> {
        return listOf(PageListContent(0, "Price",0,"Price of Home",0f, Page.LEFT.position))
    }

    override fun getRightItemList(): List<PageListContent> {
        return listOf(PageListContent(0, "Price",0,"Price of Home",0f, Page.RIGHT.position))
    }
}